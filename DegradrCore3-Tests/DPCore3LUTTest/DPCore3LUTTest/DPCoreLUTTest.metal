//
//  DPCoreLUTTest.metal
//  DPCore3LUTTest
//
//  Created by denis svinarchuk on 03.11.15.
//  Copyright © 2015 Degradr.photo. All rights reserved.
//

#include <metal_stdlib>
#include "DPMetal_main.h"
using namespace metal;

//
//
//kernel void kernel_adjustCLUT3D(texture2d<float, access::sample>  inTexture       [[texture(0)]],
//                             texture2d<float, access::write>   outTexture      [[texture(1)]],
//                             texture3d<float, access::sample>  lut             [[texture(2)]],
//                             uint2 gid [[thread_position_in_grid]]){
//    
//    constexpr sampler s(address::clamp_to_edge, filter::linear, coord::normalized);
//
//    float4 input_color   = inTexture.read(gid);
//    float4 result  = lut.sample(s, input_color.rgb);
//    
//    outTexture.write(result, gid);
//}
//
//kernel void kernel_adjustCLUT1D(texture2d<float, access::sample>  inTexture       [[texture(0)]],
//                                texture2d<float, access::write>   outTexture      [[texture(1)]],
//                                texture2d<float, access::sample>  lut             [[texture(2)]],
//                                uint2 gid [[thread_position_in_grid]]){
//    
//    constexpr sampler s(address::clamp_to_edge, filter::linear, coord::normalized);
//    
//    float4 input_color   = inTexture.read(gid);
//
//    half red   = lut.sample(s, float2(input_color.r, 0.0)).r;
//    half green = lut.sample(s, float2(input_color.g, 0.0)).g;
//    half blue  = lut.sample(s, float2(input_color.b, 0.0)).b;
//    
//    float4 result = float4(red, green, blue, 1.0);
//
//    outTexture.write(result, gid);
//}
