//
//  ViewController.swift
//  DPCore3LUTTest
//
//  Created by denis svinarchuk on 03.11.15.
//  Copyright © 2015 Degradr.photo. All rights reserved.
//

import UIKit



class ViewController: UIViewController {

    var context:DPContext!=nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        context = DPContext.newContext();
        

        autoreleasepool{
            
            do{
                let lut = try DPCubeLUTFileProvider.newLUTNamed("K64PW", context: context)
                
                let filter = DPCLUTFilter(context: context, lut: lut, type: lut.type)
                
                filter.source = DPUIImageProvider.newWithImage(UIImage(named: "test.jpg"), context: context)
                
                var image = UIImage(imageProvider: filter.destination)
                
                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                
                image = nil
                
                filter.flush()
                
                NSLog(" *** %@", filter.lutSource!)
                
            }
            catch let error as NSError{
                
                NSLog(" *** format reading error: %@", error)
                
            }
            
        }
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

