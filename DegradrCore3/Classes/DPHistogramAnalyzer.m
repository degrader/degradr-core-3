//
//  DPHistogramAnalizer.m
//  DegradrCore3
//
//  Created by denn on 14.06.15.
//  Copyright (c) 2015 Degradr.Photo. All rights reserved.
//

#import "DPHistogramAnalyzer.h"
#import "DPConstants.h"
#import "UIImage+DPImageProvider.h"
#import <Accelerate/Accelerate.h>

const NSUInteger kDP_Histogram_Size = 256;

@interface DPHistogramAnalyzer()
@property (assign,atomic) BOOL inProgress;
@property (nonatomic,strong) NSMutableArray   *priivateSolverList;
@property (nonatomic,strong) DPMathHistogram  *histogram;
@property (nonatomic,strong) id<MTLBuffer>    histogramBufferUniform;

@end

typedef struct {
    uint count;
    uint Xs[kDP_Histogram_Size];
    uint Ys[kDP_Histogram_Size];
    uint Zs[kDP_Histogram_Size];
    uint Ws[kDP_Histogram_Size];
}DPHistogramBuffer;

@implementation DPHistogramAnalyzer
{
    id<MTLBuffer>         regionUniform;
    DPFunction           *kernel_function;
    dispatch_semaphore_t  inflightSemaphore;
}

- (NSMutableArray*) priivateSolverList{
    if (!_priivateSolverList) {
        _priivateSolverList = [NSMutableArray new];
    }
    return _priivateSolverList;
}

- (void) addSolver:(id<DPHistogramSolverProtocol>)solver{
    if (solver) {
        [self.priivateSolverList addObject:solver];
    }
}

- (void) removeSolver:(id<DPHistogramSolverProtocol>)solver{
    if (solver) {
        [self.priivateSolverList removeObject:solver];
    }
}

- (void) removeAllSolvers{
    [self.priivateSolverList removeAllObjects];
}

- (NSArray*) solverList{
    return self.priivateSolverList;
}

- (void) __init__{
    [self setCenterRegionInPercent:1.0];
    inflightSemaphore = dispatch_semaphore_create(kDP_LiveViewFramePrecounting);
}

- (void) setCenterRegionInPercent:(CGFloat)region{
    _region.left   = 0.5-region/2.0f;
    _region.top    = 0.5-region/2.0f;
    _region.right  = 1.0-(0.5+region/2.0f);
    _region.bottom = 1.0-(0.5+region/2.0f);
}

- (instancetype) initWithContext:(DPContext *)aContext{
    return [self initWithHistogram: kernel_function = [DPFunction newFunction:@"kernel_histogram" context:aContext] context:aContext];
}

- (instancetype) initWithHistogram:(DPFunction*)histogramFunction context:(DPContext *)aContext{
    self = [super initWithContext:aContext];
    if (self) {
        [self addFunction:histogramFunction];
        [self __init__];
        _histogram = [[DPMathHistogram alloc] initWithChannelsType:(DPMathHistogramChannelsType_XYZW) size:kDP_Histogram_Size];
    }
    return self;
}

- (id<MTLBuffer>) histogramBufferUniform{
    //
    // histogram buffer
    //
    if (!_histogramBufferUniform) {
        _histogramBufferUniform = [self.context.device newBufferWithLength: sizeof(DPHistogramBuffer) options:MTLResourceOptionCPUCacheModeDefault];
        [_histogramBufferUniform setPurgeableState:MTLPurgeableStateKeepCurrent];
    }
    return _histogramBufferUniform;
}

- (void) configureBlitUniform:(id<MTLBlitCommandEncoder>)commandEncoder{
    NSRange range = NSMakeRange(0, sizeof(DPHistogramBuffer));
    [commandEncoder fillBuffer:self.histogramBufferUniform range:range value:0];
}

- (void) configureFunction:(DPFunction *)function uniform:(id<MTLComputeCommandEncoder>)commandEncoder{
    if (self.functionList.count>0) {
        
        [commandEncoder setBuffer:self.histogramBufferUniform offset:0 atIndex:0];
        
        //
        // region
        //
        if (!regionUniform) {
            regionUniform = [self.context.device newBufferWithLength:sizeof(_region) options:MTLResourceOptionCPUCacheModeDefault];
        }
        
        memcpy([regionUniform contents], &_region, sizeof(_region));
        
        [commandEncoder setBuffer:regionUniform offset:0 atIndex:1];
    }
}

- (void) setSource:(DPImageProvider*)source{
    dispatch_semaphore_wait(inflightSemaphore, DISPATCH_TIME_FOREVER);
    [super setSource:source];
    if (source && source.texture) {
        [self processTexture: self.texture];
    }
}

- (void) processTexture:(DPTextureRef)texture{
    
    [_histogram updateWithData:&([self.histogramBufferUniform contents][0])+sizeof(uint) dataType:DPMathHistogramDataType_NORMAL];
    
    NSUInteger width  = [texture width];
    NSUInteger height = [texture height];
    
    [self evalSolversWithImageSize:CGSizeMake(width, height)];
    
    if (self.histogramBufferUniform && self.histogramSolversFinishedBlock) {
        self.histogramSolversFinishedBlock();
    }
    dispatch_semaphore_signal(inflightSemaphore);
}

- (void) evalSolversWithImageSize:(CGSize)imageSize{
    for (id<DPHistogramSolverProtocol> solver in self.solverList) {
        [solver analyzer:self didUpdateWithHistogram:self.histogram withImageSize:imageSize];
        if (self.histogramSolverUpdatedBlock) {
            self.histogramSolverUpdatedBlock(solver, self.histogram, imageSize);
        }
    }
}


@end
