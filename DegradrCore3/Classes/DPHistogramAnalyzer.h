//
//  DPHistogramAnalyzer.h
//  DegradrCore3
//
//  Created by denn on 14.06.15.
//  Copyright (c) 2015 Degradr.Photo. All rights reserved.
//

#import "DPFilter.h"
#import "DPMath.h"

@class DPHistogramAnalyzer;

@protocol DPHistogramSolverProtocol <NSObject>
@required
- (void) analyzer:(DPHistogramAnalyzer*)analizer didUpdateWithHistogram:(DPMathHistogram *)histogram withImageSize:(CGSize)imageSize;
@end

@interface DPHistogramAnalyzer : DPFilter
/**
 * Create specific histogram analizer.
 */
- (instancetype) initWithHistogram:(DPFunction*)histogramFunction context:(DPContext *)aContext;

/**
 *  Region to analyze.
 */
@property(nonatomic,assign)  DPCropRegion  region;

/**
 * Histogram analyzer finish process block.
 */
@property(nonatomic, copy) void(^histogramSolverUpdatedBlock)(id<DPHistogramSolverProtocol> solver, DPMathHistogram *histogram, CGSize imageSize);
/**
 * It's invoked when all added solvers finished.
 */
@property(nonatomic, copy) void(^histogramSolversFinishedBlock)();

/**
 * Histogram analyzer solvers.
 */
@property(nonatomic,readonly) NSArray *solverList;

/**
 *  Region to analyze in percent of square.
 */
- (void) setCenterRegionInPercent:(CGFloat)region;

/**
 * Add solver.
 */
- (void) addSolver:(id<DPHistogramSolverProtocol>)solver;

/**
 * Remove solver.
 */
- (void) removeSolver:(id<DPHistogramSolverProtocol>)solver;

/**
 * Remove all solvers.
 */
- (void) removeAllSolvers;

@end
