//
//  DPCurveSplines.m
//  DegradrCore3
//
//  Created by denn on 19.06.15.
//  Copyright (c) 2015 Degradr.Photo. All rights reserved.
//
//  Acknowledgement:
//  https://github.com/BradLarson/GPUImage
//


#import "DPCurveSplines.h"
#import <UIKit/UIKit.h>
#import "DPMath.h"

@interface NSArray (DPGLExtention)
/**
 *  Hash lookup kye uses to find already prepared spline curve.
 *  Hash decreases precision floating in initial curve points to increase total perfomance...
 *  @return hash key
 */
- (NSString*) hashLookUp;
@end

@implementation NSArray(DPGLExtention)

- (NSString*) hashLookUp{
    
    NSMutableString *key = [[NSMutableString alloc] initWithString:@"Curve-"];
    
    for (NSValue *value in self) {
        CGPoint point = [value CGPointValue];
        [key appendFormat:@"%.2f-%.2f",point.x,point.y];
    }
    
    return key;
}

@end


@interface DPCurveSplines()
@property (nonatomic,strong) NSMutableDictionary  *splineCache;
@property (nonatomic,strong) DPContext *context;

@property (nonatomic, strong) NSArray *redCurve;
@property (nonatomic, strong) NSArray *greenCurve;
@property (nonatomic, strong) NSArray *blueCurve;

@property (nonatomic,readonly) NSArray* defaultPoints;

@end

@implementation DPCurveSplines

@synthesize texture=_texture;
@synthesize defaultPoints=_defaultPoints;
@synthesize reds=_reds, greens=_greens, blues=_blues, rgbs=_rgbs;

- (NSArray*) defaultPoints{
    if (!_defaultPoints) {
        _defaultPoints = [NSArray arrayWithObjects:
                         [NSValue valueWithCGPoint:CGPointMake(0.0, 0.0)],
                         [NSValue valueWithCGPoint:CGPointMake(0.5, 0.5)],
                         [NSValue valueWithCGPoint:CGPointMake(1.0, 1.0)],
                         nil];
    }
    return _defaultPoints;
}

- (instancetype) initWithContext:(DPContext *)aContext{
    self = [super init];
    
    if (self) {
        _context = aContext;
        _splineCache = [[NSMutableDictionary alloc] initWithCapacity:100];
    }
    
    return self;
}

- (DPTextureRef)texture{
    if (!_texture) {
        
        MTLTextureDescriptor *textureDescriptor = [MTLTextureDescriptor new];
        textureDescriptor.textureType = MTLTextureType1DArray;
        textureDescriptor.width       = 256;
        textureDescriptor.height      = 1;
        textureDescriptor.depth       = 1;
        textureDescriptor.pixelFormat = MTLPixelFormatR32Float;
        
        textureDescriptor.arrayLength = 3;
        textureDescriptor.mipmapLevelCount = 1;
        
        _texture = [self.context.device newTextureWithDescriptor:textureDescriptor];
    }
    return _texture;
}


- (Float32) step:(NSNumber*)value{
    float  m = (float)([self.texture width]-1);
    return fmin(fmax([value floatValue], 0.0f), m)/m;
}

- (void)updateToneCurveTexture{
    
    NSUInteger size = [self.texture width];
    
    if ( ([self.redCurve count] >= size) && ([self.greenCurve count] >= size) && ([self.blueCurve count] >= size))
    {
        NSArray *rgb = @[
                         self.redCurve,
                         self.greenCurve,
                         self.blueCurve
                         ];
        
        MTLRegion  region = MTLRegionMake2D(0, 0, [self.texture width], 1);
        NSUInteger bytesPerRow = [self.texture width] * sizeof(Float32);

        Float32 toneCurve[[self.texture width]];
        for (NSUInteger channelIndex=0; channelIndex<rgb.count; channelIndex++) {
            
            for (NSUInteger currentCurveIndex = 0; currentCurveIndex < size; currentCurveIndex++)
            {
                NSArray *color = rgb[channelIndex];
                toneCurve[currentCurveIndex]     = [self step:color[currentCurveIndex]];
                [self.texture replaceRegion:region mipmapLevel:0 slice:channelIndex withBytes:toneCurve bytesPerRow:bytesPerRow bytesPerImage:0];
            }
        }        
    }
}


- (NSArray*) createToneCurveForPoints:(NSArray*)pointsIn{
    NSUInteger width = [self.texture width];
    float      m = (float)(width-1);
    NSMutableArray *toneCurve = [NSMutableArray arrayWithCapacity:width];

    NSMutableArray *xPoints = [NSMutableArray new];
    
    NSMutableArray *points = [pointsIn mutableCopy];
    
    for (NSUInteger i=0; i<points.count; i++) {
        CGPoint point = [points[i] CGPointValue];
        point.x *=m;
        point.y *=m;
        points[i] = [NSValue valueWithCGPoint:point];
    }
    
    for (NSUInteger i=0; i<width; i++){
        [xPoints addObject:[NSNumber numberWithUnsignedInteger:i]];
    }
    
    NSArray *curve = [DPMathCurve make2DAdobeWithControls:points inXPoints:xPoints];
    
    for (NSUInteger i=0; i<width; i++) {
        toneCurve[i]=@([curve[i] CGPointValue].y);
    }
    
    return toneCurve;
}


#pragma mark -
#pragma mark - Composite curve
- (NSArray*) rgbs{
    if (!_rgbs) {
        _rgbs = [self.defaultPoints copy];
    }
    return _rgbs;
}

- (void) setRgbs:(NSArray *)newValue{
    _rgbs = [newValue copy];
    _redCurve = _greenCurve = _blueCurve = [self createToneCurveForPoints:_rgbs];
    [self updateToneCurveTexture];
}


#pragma mark -
#pragma mark - REDS
- (NSArray*) reds{
    if (!_reds) {
        _reds = [self.defaultPoints copy];
    }
    return _reds;
}

- (void)setReds:(NSArray *)newValue
{  
    _reds = [newValue copy];
    _redCurve = [self createToneCurveForPoints:_reds];
    [self updateToneCurveTexture];
}

- (NSArray*)redCurve{
    if (!_redCurve) {
        _redCurve = [self createToneCurveForPoints:self.reds];
    }
    return _redCurve;
}

#pragma mark -
#pragma mark - GREENS
- (NSArray*)greens{
    if (!_greens) {
        _greens = [self.defaultPoints copy];
    }
    return _greens;
}

- (void)setGreens:(NSArray *)newValue
{
    _greens = [newValue copy];
    _greenCurve = [self createToneCurveForPoints:_greens];
    [self updateToneCurveTexture];
}

- (NSArray*)greenCurve{
    if (!_greenCurve) {
        _greenCurve = [self createToneCurveForPoints:self.greens];
    }
    return _greenCurve;
}


#pragma mark - 
#pragma mark BLUES
- (NSArray*) blues{
    if (!_blues) {
        _blues=[self.defaultPoints copy];
    }
    return _blues;
}

- (void)setBlues:(NSArray *)newValue
{
    _blues = [newValue copy];
    _blueCurve = [self createToneCurveForPoints:_blues];
    [self updateToneCurveTexture];
}

- (NSArray*)blueCurve{
    if (!_blueCurve) {
        _blueCurve = [self createToneCurveForPoints:self.blues];
    }
    return _blueCurve;
}

@end
