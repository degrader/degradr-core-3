//
//  DPCLUTFilter.h
//  DegradrCore3
//
//  Created by denis svinarchuk on 04.11.15.
//  Copyright © 2015 Degradr.Photo. All rights reserved.
//

#import "DPFilter.h"
#import "DPCubeLUTFileProvider.h"

typedef struct{
    DPBlending    blending;
} DPCLUTAdjustment;

/**
 * The filter apply color lookup table (CLUT) from image provider to map source image texture to lutSource contained
 * range color.
 * It suppouses DPCLUTFilter works with DPCubeLUTFileProvider class which reads standart Adobe Cube LUT file.
 */
@interface DPCLUTFilter : DPFilter

/**
 * A source of lookup table provider. The source must be defined before filter processing will start.
 */
@property (nonatomic,strong) DPImageProvider *lutSource;

/**
 * Adjustment parameters.
 */
@property (nonatomic,assign) DPCLUTAdjustment adjustment;

/**
 * Initilize new CLUT filter object with lut provider and type of table: 1D or 3D.
 */
- (instancetype) initWithContext:(DPContext *)aContext lut:(DPImageProvider*)lut type:(DPCLUTType)type;

/**
 * Create new CLUT filter object with lut provider and type of table: 1D or 3D.
 */
- (instancetype) newWithContext:(DPContext *)aContext lut:(DPImageProvider*)lut type:(DPCLUTType)type;
@end
