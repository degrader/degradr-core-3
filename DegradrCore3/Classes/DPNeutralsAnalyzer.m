//
//  DPGrayAnalizer.m
//  DegradrCore3
//
//  Created by denn on 19.06.15.
//  Copyright (c) 2015 Degradr.Photo. All rights reserved.
//

#import "DPNeutralsAnalyzer.h"

@interface DPNeutralsSolver()
@property(nonatomic,assign)   DPNeutralClipping  clipping;
@end

@implementation DPNeutralsSolver

- (void) analyzer:(DPHistogramAnalyzer *)analizer didUpdateWithHistogram:(DPMathHistogram *)histogram withImageSize:(CGSize)imageSize{
    //
    // hues placed at start of channel W
    //
    float *huesCircle = &(histogram.data[DPMathHistogramChannel_W][0]);
    
    //
    // weights of diferrent classes (neutral) brightness is placed at the and of channel W
    //
    float *weights    = &(histogram.data[DPMathHistogramChannel_W][252]);
    
    //
    // normalize hues
    //
    float huesCircleTotal = 0;
    vDSP_sve(huesCircle, 1, &huesCircleTotal, 6);
    vDSP_vsdiv(huesCircle, 1, &huesCircleTotal, huesCircle, 1, 6);
    
    DPNeutraCircle  circle = (DPNeutraCircle){
        huesCircle[0],
        huesCircle[1],
        huesCircle[2],
        huesCircle[3],
        huesCircle[4],
        huesCircle[5]
    };
    
    
    //
    // normalize neutral weights
    //
    float weightsTotal = 0;
    vDSP_sve(weights, 1, &weightsTotal, 4);
    vDSP_vsdiv(weights, 1, &weightsTotal, weights, 1, 4);

    _weights = (DPNeutralWeights){
        weights[0],
        weights[1],
        weights[2],
        weights[3],
        circle
    };    
}

@end

@interface DPNeutralsAnalyzer()
@property (nonatomic, strong)   id<MTLBuffer>    clippingUniform;
@end

@implementation DPNeutralsAnalyzer
{
    DPFunction *kernel_function;
}

@synthesize solver = _solver;

- (void) addSolver:(id<DPHistogramSolverProtocol>)solver{
    if (solver!=self.solver) {
        [super addSolver:solver];
    }
}

- (void) removeSolver:(id<DPHistogramSolverProtocol>)solver{
    if (solver!=self.solver) {
        [super removeSolver:solver];
    }
}

- (void) removeAllFilters{
    [super removeAllFilters];
    [super addSolver:self.solver];
}

- (DPNeutralsSolver*) solver{
    if (!_solver) {
        _solver = [DPNeutralsSolver new];
    }
    return _solver;
}

- (instancetype) initWithContext:(DPContext *)aContext{
    
    self = [super initWithHistogram: kernel_function=[DPFunction newFunction:@"kernel_neutralWeights" context:aContext ] context:aContext];
    
    if (self) {        
        [self setClipping: (DPNeutralClipping){
            0.1f,
            0.1f,
            0.1f
        }];
        [super addSolver:self.solver];
    }
    
    
    return self;
}

- (void) setClipping:(DPNeutralClipping)clipping{
    _clipping = clipping;
    if (!_clippingUniform) {
        _clippingUniform  = [self.context.device newBufferWithLength:sizeof(clipping) options:MTLResourceOptionCPUCacheModeDefault];
    }
    memcpy([_clippingUniform contents], &clipping, sizeof(clipping));
}

- (void) configureFunction:(DPFunction *)function uniform:(id<MTLComputeCommandEncoder>)commandEncoder{
    if (function == kernel_function) {
        [super configureFunction:nil uniform:commandEncoder];
        [commandEncoder setBuffer:_clippingUniform offset:0 atIndex:2];
    }
}
@end
