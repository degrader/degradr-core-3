//
//  DPHistogramAvarageSolver.m
//  DegradrCore3
//
//  Created by denis svinarchuk on 15.09.15.
//  Copyright © 2015 Degradr.Photo. All rights reserved.
//

#import "DPHistogramAvarageSolver.h"

@implementation DPHistogramAvarageSolver

- (void) analyzer:(DPHistogramAnalyzer *)analizer didUpdateWithHistogram:(DPMathHistogram *)histogram withImageSize:(CGSize)imageSize{
    
    @synchronized(self) {
        for (DPMathHistogramChannel i=DPMathHistogramChannel_X; i<=DPMathHistogramChannel_W; i++) {
            _avarageColor.v[i]=[histogram meanForChannel:i]/[histogram.count[i] floatValue];
        }        
    }
}

@end
