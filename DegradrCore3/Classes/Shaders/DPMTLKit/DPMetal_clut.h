//
//  DPMetal_clut.h
//  DegradrCore3
//
//  Created by denis svinarchuk on 04.11.15.
//  Copyright © 2015 Degradr.Photo. All rights reserved.
//

#ifndef __DPMETAL_CLUT_ADJUSTMENT
#define __DPMETAL_CLUT_ADJUSTMENT

#include "DPMetal_types.h"
#include "DPMetal_stdlib.h"

using namespace metal;
using namespace dpmetal;

DPMETAL_NAMESPACE_BEGIN


METAL_FUNC float4 adjustCLUT3D(
                               float4 inColor,
                               texture3d<float, access::sample>  lut,
                               constant DPCLUTAdjustIn           &adjustment
                               ){

    constexpr sampler s(address::clamp_to_edge, filter::linear, coord::normalized);

    float4 result = lut.sample(s, inColor.rgb);
    result.rgba.a = adjustment.blending.opacity;
    
    if (adjustment.blending.mode == 0) {
        result = blendLuminosity(inColor, result);
    }
    else {// only two modes yet
        result = blendNormal(inColor, result);
    }

    return result;
}



kernel void kernel_adjustCLUT3D(
                                texture2d<float, access::sample>  inTexture       [[texture(0)]],
                                texture2d<float, access::write>   outTexture      [[texture(1)]],
                                texture3d<float, access::sample>  lut             [[texture(2)]],
                                constant DPCLUTAdjustIn          &adjustment      [[buffer(0)]],
                                uint2 gid [[thread_position_in_grid]]){
    
    float4 inColor = inTexture.read(gid);
    float4 result  = adjustCLUT3D(inColor,lut,adjustment);
    
    outTexture.write(result, gid);
}


METAL_FUNC float4 adjustCLUT1D(
                               float4 inColor,
                               texture2d<float, access::sample>  lut,
                               constant DPCLUTAdjustIn           &adjustment
                               ){
    constexpr sampler s(address::clamp_to_edge, filter::linear, coord::normalized);

    half red   = lut.sample(s, float2(inColor.r, 0.0)).r;
    half green = lut.sample(s, float2(inColor.g, 0.0)).g;
    half blue  = lut.sample(s, float2(inColor.b, 0.0)).b;
    
    float4 result = float4(red, green, blue, adjustment.blending.opacity);
    
    if (adjustment.blending.mode == 0) {
        result = blendLuminosity(inColor, result);
    }
    else {// only two modes yet
        result = blendNormal(inColor, result);
    }
    
    return result;
}

kernel void kernel_adjustCLUT1D(texture2d<float, access::sample>  inTexture       [[texture(0)]],
                                texture2d<float, access::write>   outTexture      [[texture(1)]],
                                texture2d<float, access::sample>  lut             [[texture(2)]],
                                constant DPCLUTAdjustIn          &adjustment      [[buffer(0)]],
                                uint2 gid [[thread_position_in_grid]]){
    
    float4 inColor = inTexture.read(gid);
    
    float4 result  = adjustCLUT1D(inColor,lut,adjustment);
    
    outTexture.write(result, gid);
}

DPMETAL_NAMESPACE_END

#endif
