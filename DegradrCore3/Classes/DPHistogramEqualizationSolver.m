//
//  DPHistogramEqualizationSolver.m
//  DegradrCore3
//
//  Created by denis svinarchuk on 15.09.15.
//  Copyright © 2015 Degradr.Photo. All rights reserved.
//

#import "DPHistogramEqualizationSolver.h"

@implementation DPHistogramEqualizationSolver

- (void) analyzer:(DPHistogramAnalyzer *)analizer didUpdateWithHistogram:(DPMathHistogram *)histogram withImageSize:(CGSize)imageSize{
    float cumulative = 0.0;
    DPVector3 rgb; rgb.r=0;rgb.g=0;rgb.b=0;
    for (int i=0; i<histogram.size; i++) {
        cumulative += histogram.data[DPMathHistogramChannel_W][i];
        _equalization.weights[i] = cumulative/[histogram.count[DPMathHistogramChannel_W] floatValue];
    }
}

@end
