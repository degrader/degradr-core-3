//
//  DPContrastFilter.m
//  DegradrCore3
//
//  Created by denis svinarchuk on 17/06/15.
//  Copyright (c) 2015 Degradr.Photo. All rights reserved.
//

#import "DPContrastFilter.h"

@interface DPContrastFilter()
@property (nonatomic, strong)   id<MTLBuffer>    adjustmentUniform;
@end

@implementation DPContrastFilter
{
    DPFunction *kernel_filter;
}


- (instancetype) initWithContext:(DPContext *)aContext{
    
    self = [super initWithContext:aContext];
    
    if (self) {
        [self addFunction: kernel_filter = [DPFunction newFunction:@"kernel_adjustContrast" context:self.context]];
        DPContrastAdjustment adjust;
        adjust.blending.opacity = 1.0f;
        adjust.blending.mode = DP_BLENDING_LUMINOSITY;
        adjust.minimum.r = 0.0f;
        adjust.minimum.g = 0.0f;
        adjust.minimum.b = 0.0f;
        adjust.minimum.a = 0.0f;
        adjust.maximum.r = 1.0f;
        adjust.maximum.g = 1.0f;
        adjust.maximum.b = 1.0f;
        adjust.maximum.a = 1.0f;
        self.adjustment = adjust;
    }
    
    return self;
}

- (id<MTLBuffer>) adjustmentUniform{
    if (!_adjustmentUniform) {
        _adjustmentUniform  = [self.context.device newBufferWithLength:sizeof(_adjustment) options:MTLResourceOptionCPUCacheModeDefault];
    }
    return _adjustmentUniform;
}

- (void) setAdjustment:(DPContrastAdjustment)adjustment{
    _adjustment = adjustment;
    memcpy([self.adjustmentUniform contents], &_adjustment, sizeof(_adjustment));
    self.dirty = YES;
}

- (void) configureFunction:(DPFunction *)function uniform:(id<MTLComputeCommandEncoder>)commandEncoder{    
    if (function == kernel_filter) {
        [commandEncoder setBuffer:self.adjustmentUniform offset:0 atIndex:0];
    }
}


@end
