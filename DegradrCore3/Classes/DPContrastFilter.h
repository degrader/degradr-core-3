//
//  DPContrastFilter.h
//  DegradrCore3
//
//  Created by denis svinarchuk on 17/06/15.
//  Copyright (c) 2015 Degradr.Photo. All rights reserved.
//

#import "DPLevelsFilter.h"

typedef struct{
    DPVector4     minimum;
    DPVector4     maximum;
    DPBlending    blending;
} DPContrastAdjustment;

@interface DPContrastFilter : DPFilter
@property (nonatomic) DPContrastAdjustment adjustment;
@end
