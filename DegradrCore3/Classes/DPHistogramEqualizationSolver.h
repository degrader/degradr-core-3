//
//  DPHistogramEqualizationSolver.h
//  DegradrCore3
//
//  Created by denis svinarchuk on 15.09.15.
//  Copyright © 2015 Degradr.Photo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DPHistogramAnalyzer.h"

typedef struct {    
    DPVector3 rgb[256];
    float weights[256];
}DPHistogramEqualizationWeights;

@interface DPHistogramEqualizationSolver : NSObject<DPHistogramSolverProtocol>
@property (nonatomic,readonly) DPHistogramEqualizationWeights equalization;
@end
