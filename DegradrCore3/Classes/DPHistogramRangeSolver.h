//
//  DPHistogramRangeSolver.h
//  DegradrCore3
//
//  Created by denis svinarchuk on 15.09.15.
//  Copyright © 2015 Degradr.Photo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DPHistogramAnalyzer.h"

@interface DPHistogramRangeSolver : NSObject<DPHistogramSolverProtocol>
/**
 * Shadows minimum.
 */
@property(atomic,readonly) DPVector4 min;
/**
 * Highlights maximum.
 */
@property(atomic,readonly) DPVector4 max;
/**
 *  Shadows threshold clipping, in %
 */
@property(nonatomic, assign) GLfloat shadowsClipping;
/**
 *  Highlights threshold clipping, in %
 */
@property(nonatomic, assign) GLfloat highlightsClipping;
@end
