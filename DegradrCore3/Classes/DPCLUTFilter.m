//
//  DPCLUTFilter.m
//  DegradrCore3
//
//  Created by denis svinarchuk on 04.11.15.
//  Copyright © 2015 Degradr.Photo. All rights reserved.
//

#import "DPCLUTFilter.h"

@interface DPCLUTFilter()
@property (nonatomic,strong) id<MTLBuffer> adjustmentUniform;
@end

@implementation DPCLUTFilter
{
    DPFunction *kernel_function;
}

- (instancetype) initWithContext:(DPContext *)aContext{
    //
    // must be initialized with lut source
    //
    return nil;
}

- (instancetype) initWithVertex:(NSString *)vertexFunction withFragment:(NSString *)fragmentFunction context:(DPContext *)aContext{
    //
    // must be initialized with lut source
    //
    return nil;
}

- (instancetype) initWithContext:(DPContext *)aContext lut:(DPImageProvider *)lut type:(DPCLUTType)type{
    self = [super initWithContext:aContext];
    
    if (self) {
        if (type==DP_CLUT_TYPE_1D) {
            kernel_function = [DPFunction newFunction:@"kernel_adjustCLUT1D" context:self.context];
        }
        else{
            kernel_function = [DPFunction newFunction:@"kernel_adjustCLUT3D" context:self.context];
        }
        [self addFunction:kernel_function];
        
        _lutSource = lut;
        
        DPCLUTAdjustment adj;
        
        adj.blending.mode=DP_BLENDING_NORMAL;
        adj.blending.opacity=1.0f;
        
        self.adjustment = adj;
    }
    
    return self;
}

- (instancetype) newWithContext:(DPContext *)aContext lut:(DPImageProvider *)lut type:(DPCLUTType)type{
    return [[DPCLUTFilter alloc] initWithContext:aContext lut:lut type:type];
}

- (void) configureFunction:(DPFunction *)function uniform:(id<MTLComputeCommandEncoder>)commandEncoder{
    if (kernel_function==function) {
        [commandEncoder setBuffer:self.adjustmentUniform offset:0 atIndex:0];
        [commandEncoder setTexture:self.lutSource.texture atIndex:2];
    }
}

- (id<MTLBuffer>) adjustmentUniform{
    if (!_adjustmentUniform) {
        _adjustmentUniform = [self.context.device newBufferWithLength:sizeof(_adjustment) options:MTLResourceOptionCPUCacheModeDefault];
    }
    return _adjustmentUniform;
}

- (void) setAdjustment:(DPCLUTAdjustment)adjustment{
    _adjustment = adjustment;
    memcpy([self.adjustmentUniform contents], &_adjustment, sizeof(_adjustment));
}

- (void) setLutSource:(DPImageProvider *)lutSource{
    self.dirty = YES;
    _lutSource = lutSource;
}

@end
