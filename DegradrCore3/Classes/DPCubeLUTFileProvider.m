//
//  DPCubeLUTFileProvider.m
//  DegradrCore3
//
//  Created by denis svinarchuk on 03.11.15.
//  Copyright © 2015 Degradr.Photo. All rights reserved.
//

#import "DPCubeLUTFileProvider.h"
#import "DPConstants.h"
#import "DPMath.h"


@interface NSString(DPCore3)
-(bool) isNumeric;
@end

@implementation NSString(DPCore3)

-(bool) isNumeric{
    
    static NSNumberFormatter* numberFormatter = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        numberFormatter = [[NSNumberFormatter alloc] init];
        //Set the locale to US
        [numberFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        //Set the number style to Scientific
        [numberFormatter setNumberStyle:NSNumberFormatterScientificStyle];
    });
    
    if ([numberFormatter numberFromString:self] != nil)
        return YES;
    
    return NO;
}

@end

@interface DPCubeLUTFileProvider()
@property (nonatomic,strong) NSString   *title;
@property (nonatomic,assign) DPVector3  domainMin;
@property (nonatomic,assign) DPVector3  domainMax;
@property (nonatomic,assign) NSUInteger lut3DSize;
@end

@implementation DPCubeLUTFileProvider

+(instancetype) newWithContext:(DPContext *)aContext{
    return nil;
}

+ (instancetype) newLUTNamed:(NSString *)name context:(DPContext *)aContext error:(NSError *__autoreleasing *)error{
    DPCubeLUTFileProvider *this = [super newWithContext:aContext];
    if (this) {
        NSUInteger line;
        DPCLUTParserStatus status = [this updateLUTFile:name line:&line];
        if (status) {
            
            if (error) {
                switch (status) {
                    case DP_CLUT_ERROR_NOT_FOUND:
                        *error = [[NSError alloc ] initWithDomain:@""DP_QUEUE_PREFIX"cube-lut.read"
                                                             code: status
                                                         userInfo: @{
                                                                     NSLocalizedDescriptionKey:
                                                                         [NSString
                                                                          stringWithFormat:NSLocalizedString(@"Adobe Cube LUT file %@ not found", ""),name],
                                                                     NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Adobe Cube LUT file not found", ""),
                                                                     }];
                        
                        break;
                    case DP_CLUT_ERROR_FILE_FORMAT:
                    case DP_CLUT_ERROR_WRANG_RANGE:
                    case DP_CLUT_ERROR_OUT_RANGE:
                        *error = [[NSError alloc ] initWithDomain:@""DP_QUEUE_PREFIX"cube-lut.read"
                                                             code: status
                                                         userInfo: @{
                                                                     NSLocalizedDescriptionKey:
                                                                         [NSString
                                                                          stringWithFormat:NSLocalizedString(@"Adobe Cube LUT file format error in line: %i", ""),line],
                                                                     NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"Adobe Cube LUT file format error", ""),
                                                                     }];
                        break;
                    default:
                        break;
                }
            }
            return nil;
        }
    }
    return this;
}

- (DPCLUTParserStatus) updateLUTFile:(NSString *)name line:(NSUInteger *)linenumber{
    
    @autoreleasepool {
        
        BOOL useFloatLUT = NO; // [[[UIDevice currentDevice] systemVersion] floatValue]>=9.0;
        
        _domainMin.r = 0.0;
        _domainMin.g = 0.0;
        _domainMin.b = 0.0;
        _domainMax.r = 1.0;
        _domainMax.g = 1.0;
        _domainMax.b = 1.0;
        
        NSString *cubeFile = [[NSBundle mainBundle] pathForResource:name ofType:@"cube"];
        
        if (cubeFile==nil) {
            return DP_CLUT_ERROR_NOT_FOUND;
        }
        
        NSString *contents = [NSString stringWithContentsOfFile:cubeFile encoding:NSUTF8StringEncoding error:nil];
        
        NSArray *lines = [contents componentsSeparatedByCharactersInSet: [NSCharacterSet newlineCharacterSet]];
        
        NSInteger  linenum=0;
        //NSMutableArray    *data = [NSMutableArray new];
        
        NSMutableData *dataBytes = [[NSMutableData alloc] init];
        
        BOOL isData=NO;
        
        for (NSString *line in lines) {
            linenum++;
            NSArray *words =
            [line componentsSeparatedByCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
            
            if ([line hasPrefix:@"#"] || [words[0] length]==0) {
                continue;
            }
            else{
                
                NSString *keyword = words[0];
                
                if (words.count>1) {
                    if ([[keyword uppercaseString] hasPrefix:@"TITLE"]) {
                        _title = words[1];
                    }
                    else if ([[keyword uppercaseString] hasPrefix:@"DOMAIN_MIN"]) {
                        if (words.count==4) {
                            _domainMin.r=[words[1] floatValue];
                            _domainMin.g=[words[2] floatValue];
                            _domainMin.b=[words[3] floatValue];
                        }
                        else{
                            return DP_CLUT_ERROR_FILE_FORMAT;
                        }
                    }
                    else if ([[keyword uppercaseString] hasPrefix:@"DOMAIN_MAX"]) {
                        if (words.count==4) {
                            _domainMax.r=[words[1] floatValue];
                            _domainMax.g=[words[2] floatValue];
                            _domainMax.b=[words[3] floatValue];
                        }
                        else{
                            if (linenumber) *linenumber=linenum;
                            return DP_CLUT_ERROR_FILE_FORMAT;
                        }
                    }
                    else if ([[keyword uppercaseString] hasPrefix:@"LUT_3D_SIZE"]) {
                        _lut3DSize = [words[1] floatValue];
                        _type = DP_CUBE_LUT_TYPE_3D;
                        if ( _lut3DSize < 2 || _lut3DSize > 256 ) {
                            if (linenumber) *linenumber=linenum;
                            return DP_CLUT_ERROR_OUT_RANGE;
                        }
                    }
                    else if ([[keyword uppercaseString] hasPrefix:@"LUT_1D_SIZE"]) {
                        _lut3DSize = [words[1] floatValue];
                        _type = DP_CLUT_TYPE_1D;
                        if ( _lut3DSize < 2 || _lut3DSize > 65536 ) {
                            if (linenumber) *linenumber=linenum;
                            return DP_CLUT_ERROR_OUT_RANGE;
                        }
                    }
                    else if ([[keyword uppercaseString] hasPrefix:@"LUT_1D_INPUT_RANGE"]) {
                        if (words.count==3) {
                            
                            float dmin = [words[1] floatValue];
                            float dmax = [words[2] floatValue];
                            
                            _domainMin.r=_domainMin.g=_domainMin.b=dmin;
                            _domainMax.r=_domainMax.g=_domainMax.b=dmax;
                        }
                        else{
                            if (linenumber) *linenumber=linenum;
                            return DP_CLUT_ERROR_FILE_FORMAT;
                        }
                    }
                    else if (isData || [keyword isNumeric]){
                        if (
                            (_domainMax.r-_domainMin.r)<=0
                            ||
                            (_domainMax.g-_domainMin.g)<=0
                            ||
                            (_domainMax.b-_domainMin.b)<=0
                            ) {
                            if (linenumber) *linenumber=linenum;
                            return DP_CLUT_ERROR_WRANG_RANGE;
                        }
                        
                        isData = YES;
                        
                        float denom = !useFloatLUT?255.0f:1.0;
                        
                        Float32 r = [words[0] floatValue]/(_domainMax.r-_domainMin.r)*denom;
                        Float32 g = [words[1] floatValue]/(_domainMax.g-_domainMin.g)*denom;
                        Float32 b = [words[2] floatValue]/(_domainMax.g-_domainMin.g)*denom;
                        Float32 a = denom;
                        
                        if (!useFloatLUT) {
                            uint8_t ri = (uint8_t)r;
                            uint8_t gi = (uint8_t)g;
                            uint8_t bi = (uint8_t)b;
                            uint8_t ai = (uint8_t)a;
                            [dataBytes appendBytes:&ri length:sizeof(uint8_t)];
                            [dataBytes appendBytes:&gi length:sizeof(uint8_t)];
                            [dataBytes appendBytes:&bi length:sizeof(uint8_t)];
                            [dataBytes appendBytes:&ai length:sizeof(uint8_t)];
                        }
                        else{
                            [dataBytes appendBytes:&r length:sizeof(Float32)];
                            [dataBytes appendBytes:&g length:sizeof(Float32)];
                            [dataBytes appendBytes:&b length:sizeof(Float32)];
                            [dataBytes appendBytes:&a length:sizeof(Float32)];
                        }
                    }
                }
                else{
                    if (linenumber) *linenumber=linenum;
                    return DP_CLUT_ERROR_FILE_FORMAT;
                }
            }
        }
        
        NSUInteger width  = _lut3DSize;
        NSUInteger height = _type==DP_CLUT_TYPE_1D?1:_lut3DSize;
        NSUInteger depth  = _type==DP_CLUT_TYPE_1D?1:_lut3DSize;
        
        NSUInteger componentBytes = sizeof(uint8_t);
        
        if (useFloatLUT) {
            componentBytes = sizeof(Float32);
        }
        
        NSUInteger bytesPerPixel = 4 * componentBytes;
        NSUInteger bytesPerRow   = bytesPerPixel * width;
        NSUInteger bytesPerImage = bytesPerRow * height;
        
        MTLTextureDescriptor* textureDescriptor = [MTLTextureDescriptor new];
        
        textureDescriptor.textureType = _type==DP_CLUT_TYPE_1D?MTLTextureType2D:MTLTextureType3D;
        textureDescriptor.width  = _lut3DSize;
        textureDescriptor.height = _type==DP_CLUT_TYPE_1D?1:_lut3DSize;
        textureDescriptor.depth  = _type==DP_CLUT_TYPE_1D?1:_lut3DSize;
        
        if (!useFloatLUT) {
            textureDescriptor.pixelFormat = MTLPixelFormatRGBA8Unorm;
        }
        else{
            textureDescriptor.pixelFormat = MTLPixelFormatRGBA32Float;
        }
        
        textureDescriptor.arrayLength = 1;
        textureDescriptor.mipmapLevelCount = 1;
        
        self.texture = [self.context.device newTextureWithDescriptor:textureDescriptor];
        
        MTLRegion region = _type==DP_CLUT_TYPE_1D?MTLRegionMake2D(0, 0, width, 1):MTLRegionMake3D(0, 0, 0, width, height, depth);
        
        [self.texture replaceRegion:region mipmapLevel:0 slice:0 withBytes:dataBytes.bytes bytesPerRow:bytesPerRow bytesPerImage:bytesPerImage];
        
        if (linenumber) *linenumber=linenum;
    }
    
    return DP_CLUT_ERROR_OK;
}

- (NSString *)description{
    return [NSString stringWithFormat:@"Cube LUT 1.0, TITLE:%@, TYPE:%@, DOMAIN MIN:[%f,%f,%f], DOMAIN MAX:[%f,%f,%f]",
            self.title,
            @(self.type),
            self.domainMin.r,
            self.domainMin.g,
            self.domainMin.b,
            self.domainMax.r,
            self.domainMax.g,
            self.domainMax.b
            ];
}

@end
