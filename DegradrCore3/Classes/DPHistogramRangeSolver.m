//
//  DPHistogramRangeSolver.m
//  DegradrCore3
//
//  Created by denis svinarchuk on 15.09.15.
//  Copyright © 2015 Degradr.Photo. All rights reserved.
//

#import "DPHistogramRangeSolver.h"

@interface DPHistogramRangeSolver()
@end

@implementation DPHistogramRangeSolver

- (instancetype) init{
    self = [super init];
    if (self) {
        _shadowsClipping    = 0.01f/100.0f;
        _highlightsClipping = 0.01f/100.0f;
    }
    return self;
}

- (void) analyzer:(DPHistogramAnalyzer *)analizer didUpdateWithHistogram:(DPMathHistogram *)histogram withImageSize:(CGSize)imageSize{
    @synchronized(self) {
        
        for (DPMathHistogramChannel i=DPMathHistogramChannel_X; i<=DPMathHistogramChannel_W; i++) {
            _min.v[i] = [histogram lowForChannel:i withClipping:self.shadowsClipping];
        }
        
        for (DPMathHistogramChannel i=DPMathHistogramChannel_X; i<=DPMathHistogramChannel_W; i++) {
            _max.v[i] = [histogram highForChannel:i withClipping:self.highlightsClipping];
        }
    }
}

@end
