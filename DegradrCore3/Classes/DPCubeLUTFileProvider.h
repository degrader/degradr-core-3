//
//  DPCubeLUTFileProvider.h
//  DegradrCore3
//
//  Created by denis svinarchuk on 03.11.15.
//  Copyright © 2015 Degradr.Photo. All rights reserved.
//

#import "DPImageProvider.h"
#import "DPTypes.h"

/**
 *  Texture provider is bound with Cube LUT file.
 *  Cube LUT Specification Version 1.0
 *  http://wwwimages.adobe.com/content/dam/Adobe/en/products/speedgrade/cc/pdfs/cube-lut-specification-1.0.pdf
 */
@interface DPCubeLUTFileProvider : DPImageProvider

@property (nonatomic,readonly) DPCLUTType type;

/**
 *  New texture provider associated with a LUT file with the specified name in the application’s main bundle.
 *
 *  @param fileName, the name of the specified file contains LUT table
 *  @param aContext processing context
 *
 *  @return texture provider.
 */
+ (instancetype) newLUTNamed:(NSString*)name context:(DPContext *)aContext error:(NSError**)error;

@end
