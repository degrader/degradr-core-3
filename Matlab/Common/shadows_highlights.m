%%
%
%

function z = shadows_highlights(Li,Ws,Wts,Ks,Wh,Wth,Ka)
    ys = shadows_correction(Li,Ws,Wts,Ks);
    yh = highlights_correction(Li,Wh,Wth,Ka);
    z = (Li+ys).*yh;
end