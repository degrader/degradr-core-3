%%
%
%

function z = shadows_correction(Li,W,Wt,Ks)
    z = zeros(1,length(Li));
    for i=1:length(Li)
        l = Li(i);
        c = (1.0 - ((1.0 - l).^4));
        z(1,i) = c * W/(exp(6 * l * Ks / Wt )) * Wt ;
    end
end